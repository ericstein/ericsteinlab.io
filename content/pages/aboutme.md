Title: About Me
Date: 2017-07-04

# This is my website! #

I'm Eric. In my paid time, I'm a data "alchemist"  at the Wharton School. In my free time, I do web development
with Django, cook food, and spend time outdoors.

You can find me on [Github](https://github.com/metllord) and [twitter](https://twitter.com/metllord).



